# Title: LEGv8 Assembly to Binary/Hex Converter
# Author: Kai Zhu

import re
from os import path

class bcolors:                                                          # formatting colours
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

OPCODE = {}

def MakeDict(filename: str):
    # builds dictionary from text file

    d = {}
    with open(filename) as f:
        for line in f:
            line = line.split()
            if not line or line[0] == "#":
                continue                    # skip comments
            if len(line) > 2:                     # create list as key if key has more than 1 column
                d[line[0]] = line[1:]
            else:
                d[line[0]] = line[1]
    return d

def Dec2Bin(num: str, size: int):
    # converts a register number into binary of fixed size: size
    try:
        binary = bin(int(num))[2:]
    except ValueError:
        print(bcolors.FAIL + "Invalid input where number is expected!" + bcolors.ENDC)
        return "0"*size
    else:
        binary = "0"*(size - len(binary)) + binary                          # add leading 0s
        return binary
    
        

def Bin2Hex(binary: str):
    # converts a binary sequence separated by space into a hex sequence

    out = ""
    bins = re.split(r"\s", binary)                                      # split by space
    for num in bins:
        out += hex(int(num,2))[2:]
    return out


def Op2Bin(instructions: list):
    # converts an assembly instruction to binary machine code

    #size = len(instructions)

    if instructions[0] in OPCODE:
        binary = OPCODE[instructions[0]][0]                                # lookup OpCode
    else:
        return "error"
    try:
        Rd = Dec2Bin(instructions[1][1:], 5)           # translate Rd, Rn, and Rm registers depending on format
        Rn = Dec2Bin(instructions[2][1:], 5)

        if OPCODE[instructions[0]][1] == "R":
            Rm = Dec2Bin(instructions[3][1:], 5)                                    
            binary += Rm + "0"*6 + Rn + Rd
        elif OPCODE[instructions[0]][1] == "S":
            shamt = Dec2Bin(instructions[3], 6) 
            binary += "0"*5 + shamt + Rn + Rd    
        elif OPCODE[instructions[0]][1] == "D":
            Rm = Dec2Bin(instructions[3], 9)                                      
            binary += Rm + "00" + Rn + Rd
        elif OPCODE[instructions[0]][1] == "I":
            aluimm = Dec2Bin(instructions[3], 10)
            binary += aluimm + Rn + Rd
        
        
        return ' '.join(binary[i:i+4] for i in range(0, len(binary), 4))    # add space every 4 chars
    except IndexError:
        print(bcolors.FAIL + "Invalid # of parameters!" + bcolors.ENDC)
        return "error"

def translateFile(filename):
    if not path.exists(filename):
        print(bcolors.FAIL + "Input file not found! Syntax: RF <filename>" + bcolors.ENDC)
        return
    with open(filename) as fin:
        with open(filename[:-4] + "_OUT.txt", "w") as fout:
            for i, line in enumerate(fin):
                line = line.strip()
                # if not line or line[0]=="#":
                #     fout.write(line + "\n")
                # else:
                instructions = re.split(r'\W+',line)
                binary = Op2Bin(instructions)
                if binary != "error":
                    h = Bin2Hex(binary)
                    fout.write(binary + "    " + h + "\n")
                    print(bcolors.OKCYAN + f"{i}: " + bcolors.ENDC, binary + "    " + bcolors.OKGREEN + h + bcolors.ENDC)
                else:
                    fout.write(line + "\n")



def main():
    global OPCODE
    OPCODE = MakeDict("OPCODE.txt")
    while True:
        print()
        op = input(bcolors.OKGREEN + "Assembly instruction (EXIT to end, RF to read file): " + bcolors.ENDC).upper()
        instructions = re.split(r'\W+',op)                                  # split instruction

        if instructions[0] == "EXIT" or instructions[0] == "QUIT":
            return
        elif instructions[0] == "RF":                                       # read from file
            print(bcolors.OKCYAN + "Reading from file: " + bcolors.ENDC + '.'.join(instructions[1:]))
            if path.exists(op[3:-4]+"_OUT.txt"):
                if input(bcolors.WARNING + op[3:-4]+"_out.txt already exists, enter Y to confirm overwrite, anything else to cancel: " + bcolors.ENDC).upper() == "Y":
                    translateFile(op[3:])
            else:
                translateFile(op[3:])
            continue
        binary = Op2Bin(instructions)
        if binary != "error":
            print(bcolors.OKCYAN + "Bin: " + bcolors.ENDC, binary)
            print(bcolors.OKCYAN + "Hex: " + bcolors.ENDC, Bin2Hex(binary))
        else:
            print(bcolors.FAIL + "Invalid opcode!")

if __name__ == '__main__':
    main()