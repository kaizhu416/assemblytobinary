# LEGv8 Assembly Code to Binary/Hex Converter #
By Kai Zhu

### What does this do? ###
This Python program converts assembly into binary machine code.

### Why? ###
* For science!
* Great way to learn assembly code, instruction formatting, and their binary equivalents

### Current Features ###

* Translate R, I, and D instruction sets
* Formats and displays 32 bit binary machine code
* Displays 8 byte hex equivalent
* Reads from OPCODE dictionary file
* Option to read input from file and output to _out.txt file

### To-dos ###

* Add rest of the instructions to OPCODE.txt
* GUI or web-based interface